# Avaliação - SCC4

Tecnologias backend
---------------
JAVA 8, Maven, Spring Boot e MySQL.

Tecnologias frontend
---------------
Angular 8.3.23, Angular Material, Flex-Laout, NgxMask e RxJS 6.4.0


Cadastro de destinatários
---------------
O cadastro de destinatários é realizado através do upload de um arquivo no formato CSV. A leitura do arquivo leva me conta cada atributo separado por vírgula ou quebra de linha.
Ordem dos atributos: Nome, Cpf(Com ou sem formatação), Telefone, Endereço e E-mail.

**Exemplo do padrão de entrada de dados:**
```
Paulo,39941538085,(64) 9 9962-8888,Sem endereço,paulo@gmail.com
Lucas,99635639023,(64) 9 9962-8888,Sem endereço,lucas@gmail.com
Marcos,92657551050,(64) 9 9962-8888,Sem endereço,marcos@gmail.com
```

Regras de negócio
---------------
* Os atributos CPF e E-mail são UniqueConstraint e portanto não podem repetidos.
* A aplicação realiza a validação se o CPF informado no cadastro de Remetente ou Destinatário é um CPF válido.
* Uma Mensagem deve conter um Remetente e um ou mais Destinatários
* O servidor imprime no console da aplicação quando uma Mensagem é envida. Ex: João enviou mensagem para Paulo, Lucas e Marcos;
* A camada Task de Controller e Service no server, realiza operações de manipulação de listas, texto e números. Também possuí uma lógica de saque para contagem do menor número de cédulas levando em conta somente cédulas de R$3 e R$5.
* A classe Fracao.java é uma calculadora que realiza operações aritméticas (soma, subtração, multiplicação e divisão) em frações e devolve o resultado em frações.

Portas utilizadas
---------------
```
Server: 8080
Client: 4200
```

Imagens
---------------
![API](imagens/Consulta Remetentes.PNG)
![API](imagens/Enviar Mensagem.png)
![API](imagens/Cadastro Destinatários.PNG)


