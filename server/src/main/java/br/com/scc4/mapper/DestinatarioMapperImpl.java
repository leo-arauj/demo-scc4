package br.com.scc4.mapper;

import org.springframework.stereotype.Component;

import br.com.scc4.dto.DestinatarioDTO;
import br.com.scc4.model.Destinatario;

/**
 * Classe de implementação da interface {@link DestinatarioMapper}
 * 
 * @author Leonardo Araújo
 */
@Component
public class DestinatarioMapperImpl implements DestinatarioMapper {

	@Override
	public DestinatarioDTO toDTO(final Destinatario entity) {
		if(entity == null) {
			return null;
		}

		DestinatarioDTO dto = new DestinatarioDTO();

		dto.setId(entity.getId());
		dto.setNome(entity.getNome());
		dto.setCpf(entity.getCpf());
		dto.setTelefone(entity.getTelefone());
		dto.setEndereco(entity.getEndereco());
		dto.setEmail(entity.getEmail());

		return dto;
	}

	@Override
	public Destinatario toEntity(final DestinatarioDTO dto) {
		if(dto == null) {
			return null;
		}

		Destinatario entity = new Destinatario();

		entity.setId(dto.getId());
		entity.setNome(dto.getNome());
		entity.setCpf(dto.getCpf());
		entity.setTelefone(dto.getTelefone());
		entity.setEndereco(dto.getEndereco());
		entity.setEmail(dto.getEmail());

		return entity;
	}

}
