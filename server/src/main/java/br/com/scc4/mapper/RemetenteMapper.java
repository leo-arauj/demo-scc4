package br.com.scc4.mapper;

import br.com.scc4.dto.RemetenteDTO;
import br.com.scc4.model.Remetente;

/**
 * Classe adapter referente a entidade {@link Remetente}.
 * 
 * @author Leonardo Araújo
 */
public interface RemetenteMapper {

	/**
	 * Converte a entidade {@link Remetente} em DTO {@link RemetenteDTO}
	 * 
	 * @param remetente
	 * @return
	 */
	public RemetenteDTO toDTO(final Remetente remetente);

	/**
	 * Converte o DTO {@link RemetenteDTO} para entidade {@link Remetente}
	 * 
	 * @param clienteDTO
	 * @return
	 */
	public Remetente toEntity(final RemetenteDTO remetenteDTO);
}
