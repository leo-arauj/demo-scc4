package br.com.scc4.mapper;

import br.com.scc4.dto.DestinatarioDTO;
import br.com.scc4.model.Destinatario;

/**
 * Classe adapter referente a entidade {@link Destinatario}.
 * 
 * @author Leonardo Araújo
 */
//@Component
public interface DestinatarioMapper {

	/**
	 * Converte a entidade {@link Destinatario} em DTO {@link DestinatarioDTO}
	 * 
	 * @param destinatario
	 * @return
	 */
	public DestinatarioDTO toDTO(final Destinatario destinatario);

	/**
	 * Converte o DTO {@link DestinatarioDTO} para entidade {@link Destinatario}
	 * 
	 * @param destinatarioDTO
	 * @return
	 */
	public Destinatario toEntity(final DestinatarioDTO destinatarioDTO);
}