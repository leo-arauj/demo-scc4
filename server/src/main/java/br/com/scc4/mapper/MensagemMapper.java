package br.com.scc4.mapper;

import br.com.scc4.dto.MensagemDTO;
import br.com.scc4.model.Mensagem;

/**
 * Classe adapter referente a entidade {@link Mensagem}.
 * 
 * @author Leonardo Araújo
 */
public interface MensagemMapper {

	/**
	 * Converte a entidade {@link Mensagem} em DTO {@link MensagemDTO}
	 * 
	 * @param mensagem
	 * @return
	 */
	public MensagemDTO toDTO(final Mensagem mensagem);

	/**
	 * Converte o DTO {@link MensagemDTO} para entidade {@link Mensagem}
	 * 
	 * @param mensagemDTO
	 * @return
	 */
	public Mensagem toEntity(final MensagemDTO mensagemDTO);
}
