package br.com.scc4.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.scc4.dto.DestinatarioDTO;
import br.com.scc4.dto.MensagemDTO;
import br.com.scc4.model.Destinatario;
import br.com.scc4.model.Mensagem;

/**
 * Classe de implementação da interface {@link MensagemMapper}
 * 
 * @author Leonardo Araújo
 */
@Component
public class MensagemMapperImpl implements MensagemMapper {

	@Autowired
	private RemetenteMapper remetenteMapper;

	@Autowired
	private DestinatarioMapper destinatarioMapper;


	@Override
	public MensagemDTO toDTO(final Mensagem mensagem) {
		if(mensagem == null) {
			return null;
		}

		MensagemDTO dto = new MensagemDTO();

		dto.setId(mensagem.getId());
		dto.setMensagem(mensagem.getMensagem());
		dto.setRemetente( remetenteMapper.toDTO(mensagem.getRemetente()) );
		dto.setDestinatarios( destinatarioListToDestinatarioDTOList(mensagem.getDestinatarios()) );

		return dto;
	}

	@Override
	public Mensagem toEntity(final MensagemDTO dto) {
		if(dto == null) {
			return null;
		}

		Mensagem mensagem = new Mensagem();

		mensagem.setId(dto.getId());
		mensagem.setMensagem(dto.getMensagem());
		mensagem.setRemetente( remetenteMapper.toEntity(dto.getRemetente()) );
		mensagem.setDestinatarios( destinatarioDTOListToDestinatarioList(dto.getDestinatarios()) );

		return mensagem;
	}

	/**
	 * Converte uma lista de {@link Destinatario} em uma nova lista de {@link DestinatarioDTO}
	 * 
	 * @param list
	 * @return
	 */
	protected List<DestinatarioDTO> destinatarioListToDestinatarioDTOList(final List<Destinatario> list) {
		if(list == null || list.isEmpty()) {
			return null;
		}
		
		List<DestinatarioDTO> dtos = new ArrayList<DestinatarioDTO>( list.size() );
		for( Destinatario destinatario : list ) {
			dtos.add( destinatarioMapper.toDTO(destinatario) );
		}
		
		return dtos;
	}

	/**
	 * Converte uma lista de {@link DestinatarioDTO} em uma nova lista de {@link Destinatario}
	 * 
	 * @param list
	 * @return
	 */
	protected List<Destinatario> destinatarioDTOListToDestinatarioList(final List<DestinatarioDTO> list) {
		if(list == null || list.isEmpty()) {
			return null;
		}

		List<Destinatario> destinatarios = new ArrayList<Destinatario>( list.size() );
		for( DestinatarioDTO dto : list ) {
			destinatarios.add( destinatarioMapper.toEntity(dto) );
		}

		return destinatarios;
	}

}
