package br.com.scc4.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.scc4.dto.MensagemDTO;
import br.com.scc4.dto.RemetenteDTO;
import br.com.scc4.model.Mensagem;
import br.com.scc4.model.Remetente;

/**
 * Classe de implementação da interface {@link RemetenteMapper}
 * 
 * @author Leonardo Araújo
 */
@Component
public class RemetenteMapperImpl implements RemetenteMapper {
	
	@Autowired
	private MensagemMapper mensagemMapper;


	@Override
	public RemetenteDTO toDTO(final Remetente remetente) {
		if(remetente == null) {
			return null;
		}

		RemetenteDTO remetenteDTO = new RemetenteDTO();

		remetenteDTO.setId(remetente.getId());
		remetenteDTO.setNome(remetente.getNome());
		remetenteDTO.setCpf(remetente.getCpf());
		remetenteDTO.setTelefone(remetente.getTelefone());
		remetenteDTO.setEndereco(remetente.getEndereco());
		remetenteDTO.setEmail(remetente.getEmail());

		return remetenteDTO;
	}

	@Override
	public Remetente toEntity(RemetenteDTO remetenteDTO) {
		if(remetenteDTO == null) {
			return null;
		}

		Remetente remetente = new Remetente();

		remetente.setId(remetenteDTO.getId());
		remetente.setNome(remetenteDTO.getNome());
		remetente.setCpf(remetenteDTO.getCpf());
		remetente.setTelefone(remetenteDTO.getTelefone());
		remetente.setEndereco(remetenteDTO.getEndereco());
		remetente.setEmail(remetenteDTO.getEmail());

		return remetente;
	}

	/**
	 * Converte uma lista de {@link Mensagem} em uma nova lista de {@link MensagemDTO}
	 * 
	 * @param list
	 * @return
	 */
	public List<MensagemDTO> mensagemListToMensagemDTOList(final List<Mensagem> list) {
		if(list == null || list.isEmpty()) {
			return null;
		}

		List<MensagemDTO> dtos = new ArrayList<MensagemDTO>( list.size() );
		for( Mensagem mensagem : list) {
			dtos.add( mensagemMapper.toDTO(mensagem) );
		}

		return dtos;
	}

	/**
	 * Converte uma lista de {@link MensagemDTO} em uma nova lista de {@link Mensagem}
	 * 
	 * @param list
	 * @return
	 */
	public List<Mensagem> mensagemDTOListToMensagemList(final List<MensagemDTO> list) {
		if(list == null || list.isEmpty()) {
			return null;
		}

		List<Mensagem> mensagens = new ArrayList<Mensagem>( list.size() );
		for( MensagemDTO dto : list ) {
			mensagens.add( mensagemMapper.toEntity(dto) );
		}

		return mensagens;
	}

}
