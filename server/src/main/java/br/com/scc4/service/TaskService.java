package br.com.scc4.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.scc4.exception.ResponseMessageCode;
import br.com.scc4.exception.ResponseMessageException;

/**
 * Classe de implementação das regras de negócio
 * 
 * @author Leonardo Araújo
 */
@Service
@Transactional(rollbackOn = ResponseMessageException.class)
public class TaskService {

	private static final String vogais = "aeiou";

	/**
	 * Recebe uma lista de inteiros e inverte a ordem dos elementos
	 * 
	 * @param lista
	 * @return
	 */
	public List<Integer> getListaReversa(final List<Integer> lista) {
		if(lista.isEmpty()) {
			throw new ResponseMessageException(ResponseMessageCode.CAMPOS_OBRIGATORIOS_NAO_INFORMADOS);
		}

		Collections.reverse(lista);

		return lista;
	}

	/**
	 * Recebe uma lista de inteiros e retorna somente os números impares
	 * 
	 * @param lista
	 * @return
	 */
	public List<Integer> getNumerosImpares(final List<Integer> lista) {
		if(lista.isEmpty()) {
			throw new ResponseMessageException(ResponseMessageCode.CAMPOS_OBRIGATORIOS_NAO_INFORMADOS);
		}

		return lista.stream().filter(o -> o % 2 != 0).collect(Collectors.toList());
	}

	/**
	 * Recebe uma lista de inteiros e retorna somente os números pares
	 * 
	 * @param lista
	 * @return
	 */
	public List<Integer> getNumerosPares(final List<Integer> lista) {
		if(lista.isEmpty()) {
			throw new ResponseMessageException(ResponseMessageCode.CAMPOS_OBRIGATORIOS_NAO_INFORMADOS);
		}

		return lista.stream().filter(o -> o % 2 == 0).collect(Collectors.toList());
	}

	/**
	 * Recebe uma String como parâmetro e retorna o tamanho dela
	 * 
	 * @param palavra
	 * @return
	 */
	public String getTamanhoPalavra(final String palavra) {
		if(StringUtils.isEmpty(palavra)) {
			throw new ResponseMessageException(ResponseMessageCode.CAMPOS_OBRIGATORIOS_NAO_INFORMADOS);
		}

		StringBuilder builder = new StringBuilder();
		builder.append("tamanho=");
		builder.append(palavra.length());

		return builder.toString();
	}

	/**
	 * Recebe uma String como parâmetro e retorna uma String com todas as letras maiúsculas
	 * 
	 * @param palavra
	 * @return
	 */
	public String getPalavraToUpperCase(final String palavra) {
		if(StringUtils.isEmpty(palavra)) {
			throw new ResponseMessageException(ResponseMessageCode.CAMPOS_OBRIGATORIOS_NAO_INFORMADOS);
		}

		return palavra.toUpperCase();
	}

	/**
	 * Converte um nome em nome bibliográfico
	 * 
	 * @param nome
	 * @return
	 */
	public String getNomeBibliografico(final String nome) {
		List<String> palavras = Arrays.asList(nome.split(" "));

		int lastIndex = palavras.size() - 1;
		String ultimoNome = palavras.get(lastIndex);

		StringBuilder builder = new StringBuilder();
		builder.append(ultimoNome.toUpperCase());
		builder.append(", ");

		for(int i = 0; i < lastIndex; i++) {
			builder.append(palavras.get(i));

			if(i < lastIndex - 1) {
				builder.append(" ");
			}
		}

		return builder.toString();
	}
	
	/**
	 * Recebe uma String como parâmetro e retorna as <b>vogais</b> contidas na String
	 * 
	 * @param palavra
	 * @return
	 */
	public String getVogais(final String palavra) {
		StringBuilder builder = new StringBuilder();

		ArrayList<Character> text = convertTextToArray(palavra);
		List<Character> listOfVogais = getListVogais();

		text.forEach(o -> {
			if(listOfVogais.contains(o)) {
				builder.append(o);
			}
		});

		return builder.toString();
	}

	/**
	 * Recebe uma String como parâmetro e retorna as <b>consoantes</b> contidas na String
	 * 
	 * @param palavra
	 * @return
	 */
	public String getConsoantes(final String palavra) {
		StringBuilder builder = new StringBuilder();

		ArrayList<Character> text = convertTextToArray(palavra);
		List<Character> listOfVogais = getListVogais();

		text.forEach(o -> {
			if(!listOfVogais.contains(o)) {
				builder.append(o);
			}
		});

		return builder.toString();
	}

	/**
	 * Transforma uma String em um <b>ArrayList</b> de <b>Character</b>
	 * 
	 * @param text
	 * @return ArrayList
	 */
	private ArrayList<Character> convertTextToArray(final String text) {
		if(StringUtils.isEmpty(text)) {
			throw new ResponseMessageException(ResponseMessageCode.CAMPOS_OBRIGATORIOS_NAO_INFORMADOS);
		}

		return new ArrayList<>(
								text.toLowerCase().chars()
								.mapToObj(e -> (char) e)
								.collect(Collectors.toList()));
	}

	/**
	 * Retorna uma lista com todas as <b>vogais</b>
	 * 
	 * @return List
	 */
	private List<Character> getListVogais() {
		List<Character> listOfVogais = new ArrayList<>();

		for(char v : vogais.toCharArray()) {
			listOfVogais.add(v);
		}

		return listOfVogais;
	}

	/**
	 * Função que retorna quantas cédulas mínimas de R$3 e R$5 serão necessárias para o saque
	 * 
	 * @param valor
	 * @return
	 */
	public String getSaque(int valor) {
		int valorTemp = valor;

		int totalNotas5 = 0;
		int totalNotas3 = 0;

		while(valorTemp >= 8) {
			if(valorTemp % 5 == 0) {
				totalNotas5 = valorTemp / 5;
				valorTemp = valorTemp - (totalNotas5 * 5);
			} else {
				totalNotas3++;
				valorTemp = valorTemp - 3;
			}
		}

		return getExtratoSaque(totalNotas3, totalNotas5, valor);
	}

	/**
	 * Gera uma string com todos os dados do cálculo de cédulas para o saque
	 * 
	 * @param totalNotas3
	 * @param totalNotas5
	 * @param valor
	 * @return
	 */
	private String getExtratoSaque(final int totalNotas3, final int totalNotas5, final int valor) {
		StringBuilder builder = new StringBuilder();

		builder.append("Saque R$");
		builder.append(valor);
		builder.append(": ");

		if(totalNotas3 > 0) {
			builder.append(totalNotas3);
			builder.append((totalNotas3 > 1) ? " notas de R$3" : " nota de R$3");
		}

		if(totalNotas5 > 0) {
			if(totalNotas3 > 0) {
				builder.append(" e ");
			}
			builder.append(totalNotas5);
			builder.append((totalNotas5 > 1) ? " notas de R$5" : " nota de R$5");
		}

		return builder.toString();
	}

}