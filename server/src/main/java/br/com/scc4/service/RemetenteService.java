package br.com.scc4.service;

import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.scc4.exception.ResponseMessageCode;
import br.com.scc4.exception.ResponseMessageException;
import br.com.scc4.model.Remetente;
import br.com.scc4.repository.RemetenteRepository;
import br.com.scc4.util.CpfUtil;

/**
 * Classe de implementação das regras de negócio referente a {@link Remetente}
 * 
 * @author Leonardo Araújo
 */
@Service
@Transactional(rollbackOn = ResponseMessageException.class)
public class RemetenteService {

	@Autowired
	private RemetenteRepository remetenteRepository;


	/**
	 * Realiza as validações e persistência de {@link Remetente}
	 * 
	 * @param remetente
	 * @return
	 */
	public Remetente salvar(final Remetente remetente) {
		validarCamposObrigatorios(remetente);
		validarCpf(remetente.getCpf());

		String cpfFormatado = formatarCpf(remetente.getCpf());
		remetente.setCpf(cpfFormatado);

		if(Objects.isNull(remetente.getId())) {
			validarDuplicidadeCpf(remetente.getCpf());
			validarDuplicidadeEmail(remetente.getEmail());
		} else {
			validarDuplicidadeForUpdate(remetente);
		}

		return remetenteRepository.save(remetente);
	}

	/**
	 * Valida duplicidade de CPF e E-mail em caso de <b>update</b> de {@link Remetente}
	 * 
	 * @param remetente
	 */
	private void validarDuplicidadeForUpdate(final Remetente remetente) {
		Remetente remetenteDB = remetenteRepository.findById(remetente.getId()).get();

		if(!remetente.getCpf().equals(remetenteDB.getCpf())) {
			validarDuplicidadeCpf(remetente.getCpf());
		}

		if(!remetente.getEmail().equals(remetenteDB.getEmail())) {
			validarDuplicidadeEmail(remetente.getEmail());
		}
	}

	/**
	 * Excluí um {@link Remetente} da base de dados
	 * 
	 * @param id
	 */
	public void excluirRemetente(final Long id) {
		if(!Objects.isNull(id)) {
			remetenteRepository.deleteById(id);
		}
	}

	/**
	 * Retorna uma lista com todos os {@link Remetente}
	 * @return
	 */
	public List<Remetente> getTodos() {
		return remetenteRepository.findAll();
	}

	/**
	 * Busca {@link Remetente} por ID
	 * 
	 * @param id
	 * @return
	 */
	public Remetente getRemetenteById(final Long id) {
		return Objects.isNull(id) ? null : remetenteRepository.findById(id).get();
	}

	/**
	 * Busca o {@link Remetente} correspondente ao Email informado
	 * 
	 * @param email
	 * @return
	 */
	public Remetente getRemetenteByEmail(final String email) {
		if(StringUtils.isEmpty(email)) {
			throw new ResponseMessageException(ResponseMessageCode.CAMPOS_OBRIGATORIOS_NAO_INFORMADOS);
		}

		return remetenteRepository.findByEmail(email);
	}

	/**
	 * Realiza a validação de duplicidade de <b>CPF</b>
	 * 
	 * @param cpf
	 */
	private void validarDuplicidadeCpf(final String cpf) {
		if(remetenteRepository.countByCpf(cpf) > 0) {
			throw new ResponseMessageException(ResponseMessageCode.DUPLICIDADE_CPF);
		}
	}
	
	/**
	 * Realiza a validação de duplicidade de <b>EMAIL</b>
	 * 
	 * @param cpf
	 */
	private void validarDuplicidadeEmail(final String email) {
		if(remetenteRepository.countByEmail(email) > 0) {
			throw new ResponseMessageException(ResponseMessageCode.DUPLICIDADE_EMAIL);
		}
	}

	/**
	 * Realiza a validação dos digitos de CPF
	 * 
	 * @param cpf
	 */
	private void validarCpf(final String cpf) {
		if(StringUtils.isEmpty(cpf)) {
			throw new ResponseMessageException(ResponseMessageCode.CAMPOS_OBRIGATORIOS_NAO_INFORMADOS);
		}

		if(!CpfUtil.isValid(cpf)) {
			throw new ResponseMessageException(ResponseMessageCode.CPF_INVALIDO);
		}
	}

	/**
	 * Realiza a formatação padrão do <b>CPF</b> de {@link Remetente}
	 * <dd><i> Ex: 000.000.000-00</i></dd>
	 * 
	 * @param cpf
	 * @return CPF formatado com a mascara padrão
	 */
	private String formatarCpf(final String cpf) {
		return CpfUtil.formatarCpf(cpf);
	}

	/**
	 * Realiza a validação de campos obrigatórios da entidade {@link Remetente}
	 * 
	 * @param remetente
	 */
	private void validarCamposObrigatorios(final Remetente remetente) {
		if(Objects.isNull(remetente)
					|| StringUtils.isEmpty(remetente.getNome())
					|| StringUtils.isEmpty(remetente.getCpf())
					|| StringUtils.isEmpty(remetente.getTelefone())
					|| StringUtils.isEmpty(remetente.getEndereco())
					|| StringUtils.isEmpty(remetente.getEmail())) {

			throw new ResponseMessageException(ResponseMessageCode.CAMPOS_OBRIGATORIOS_NAO_INFORMADOS);
		}
	}
}
