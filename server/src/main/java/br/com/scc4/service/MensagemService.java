package br.com.scc4.service;

import java.util.Objects;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.scc4.exception.ResponseMessageCode;
import br.com.scc4.exception.ResponseMessageException;
import br.com.scc4.model.Mensagem;
import br.com.scc4.repository.MensagemRepository;

/**
 * Classe de implementação das regras de negócio referente a {@link Mensagem}
 * 
 * @author Leonardo Araújo
 */
@Service
@Transactional(rollbackOn = ResponseMessageException.class)
public class MensagemService {

	@Autowired
	private MensagemRepository mensagemRepository;


	/**
	 * Realiza as validações e persistência de {@link Mensagem}
	 * 
	 * @param mensagem
	 * @return
	 */
	public Mensagem salvar(final Mensagem mensagem) {
		validarCamposObrigatorios(mensagem);

		imprimirMensagemConsole(mensagem);

		return mensagemRepository.save(mensagem);
	}

	/**
	 * Imprime no console quando uma {@link Mensagem} é enviada
	 * Ex: joão enviou mensagem para paula;
	 * 
	 * @param mensagem
	 */
	private void imprimirMensagemConsole(final Mensagem mensagem) {
		StringBuilder builder = new StringBuilder();
		builder.append(mensagem.getRemetente().getNome());
		builder.append(" enviou mensagem para ");

		int lastIndex = mensagem.getDestinatarios().size() - 1;

		for(int i = 0; i <= lastIndex; i++) {
			builder.append(mensagem.getDestinatarios().get(i).getNome());

			if(i <= lastIndex - 1) {
				builder.append(", ");
			} else {
				builder.append(";");
			}
		}

		System.out.println(builder.toString());
	}

	/**
	 * Realiza a validação de campos obrigatórios da entidade {@link Mensagem}
	 * 
	 * @param mensagem
	 */
	private void validarCamposObrigatorios(final Mensagem mensagem) {
		if(Objects.isNull(mensagem)
				|| Objects.isNull(mensagem.getRemetente())
				|| Objects.isNull(mensagem.getDestinatarios())
				|| mensagem.getDestinatarios().isEmpty()
				|| StringUtils.isEmpty(mensagem.getMensagem())) {

			throw new ResponseMessageException(ResponseMessageCode.CAMPOS_OBRIGATORIOS_NAO_INFORMADOS);
		}
	}
}
