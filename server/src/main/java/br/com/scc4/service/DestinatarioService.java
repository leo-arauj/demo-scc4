package br.com.scc4.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import br.com.scc4.exception.ResponseMessageCode;
import br.com.scc4.exception.ResponseMessageException;
import br.com.scc4.model.Destinatario;
import br.com.scc4.repository.DestinatarioRepository;
import br.com.scc4.util.CpfUtil;

/**
 * Classe de implementação das regras de negócio referente a {@link Destinatario}
 * 
 * @author Leonardo Araújo
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DestinatarioService {
	
	@Autowired
	private DestinatarioRepository destinatarioRepository;


	/**
	 * Realiza as validações e persistência de {@link Destinatario}
	 * 
	 * @param destinatario
	 * @return
	 */
	public Destinatario salvar(final Destinatario destinatario) {
		validarCamposObrigatorios(destinatario);
		validarCpf(destinatario.getCpf());

		formatarCpfDestinatario(destinatario);

		if(Objects.isNull(destinatario.getId())) {
			validarDuplicidadeCpf(destinatario.getCpf());
			validarDuplicidadeEmail(destinatario.getEmail());
		}

		return destinatarioRepository.save(destinatario);
	}

	/**
	 * Salva os destinatários contidos no arquivo CSV
	 * 
	 * @param inputStream
	 */
	public void salvarDestinatariosOfFileCSV(MultipartFile file) {
		if(file == null) {
			throw new ResponseMessageException(ResponseMessageCode.ARQUIVO_NAO_PADRAO_CSV);
		}

		List<Destinatario> list = readDestinatariosOfFileCSV(file);

		if(!list.isEmpty()) {
			list.forEach(destinatario -> {
				formatarCpfDestinatario(destinatario);
			});

			validarDuplicidadeCpfDestinatario(list);
			validarDuplicidadeEmailDestinatario(list);

			validarDuplicidadeEmailOfArquivoCSV(list);
			validarDuplicidadeCpfOfArquivoCSV(list);

			destinatarioRepository.saveAll(list);
		}
	}

	/**
	 * Realiza a validação de duplicidade de e-mail nos dados importados do arquivo .csv
	 * 
	 * @param list
	 */
	private void validarDuplicidadeEmailOfArquivoCSV(final List<Destinatario> list) {
		List<String> emails = list.stream().map(Destinatario::getEmail).collect(Collectors.toList());

		for(String e : emails) {
			int emailRepetido = 0;

			for(String o : emails) {
				if(o.contains(e)) {
					emailRepetido++;
				}
			}

			if(emailRepetido > 1) {
				throw new ResponseMessageException(ResponseMessageCode.DUPLICIDADE_EMAIL_CSV);
			}
		}
	}

	/**
	 * Realiza a validação de duplicidade de CPF nos dados importados do arquivo .csv
	 * @param list
	 */
	private void validarDuplicidadeCpfOfArquivoCSV(final List<Destinatario> list) {
		List<String> cpfs = list.stream().map(Destinatario::getCpf).collect(Collectors.toList());

		for(String e : cpfs) {
			int cpfRepetido = 0;

			for(String o : cpfs) {
				if(o.contains(e)) {
					cpfRepetido++;
				}
			}

			if(cpfRepetido > 1) {
				throw new ResponseMessageException(ResponseMessageCode.DUPLICIDADE_CPF_CSV);
			}
		}
	}

	/**
	 * Retorna uma lista com todos os {@link Destinatario}
	 * @return
	 */
	public List<Destinatario> getTodos() {
		return destinatarioRepository.findAll();
	}

	/**
	 * Busca o {@link Destinatario} correspondente ao Email informado
	 * 
	 * @param email
	 * @return
	 */
	public Destinatario getDestinatarioByEmail(final String email) {
		if(StringUtils.isEmpty(email)) {
			throw new ResponseMessageException(ResponseMessageCode.CAMPOS_OBRIGATORIOS_NAO_INFORMADOS);
		}

		return destinatarioRepository.findByEmail(email);
	}

	/**
	 * Realiza a leitura de arquivo CSV e retorna uma lista com {@link Destinatario}
	 * segundo os dados cadastrais no arquivo
	 * 
	 * @param inputStream
	 * @return
	 */
	private List<Destinatario> readDestinatariosOfFileCSV(MultipartFile file) {
		List<Destinatario> destinatarios = new ArrayList<Destinatario>();

		try {
			InputStream inputStream = file.getInputStream();
			Scanner scanner = new Scanner(inputStream).useDelimiter("\\,|\\n");

			while(scanner.hasNextLine()) {
				Destinatario destinatario = new Destinatario();

				destinatario.setNome(scanner.next());
				destinatario.setCpf(scanner.next());
				destinatario.setTelefone(scanner.next());
				destinatario.setEndereco(scanner.next());
				destinatario.setEmail(scanner.next());

				destinatarios.add(destinatario);
			}
		} catch (IllegalStateException  | IOException | NoSuchElementException e) {
			throw new ResponseMessageException(ResponseMessageCode.ERRO_LEITURA_ARQUIVO_CSV);
		}

		return destinatarios;
	}

	/**
	 * Realiza a validação de duplicidade de <b>CPF</b>
	 * 
	 * @param cpf
	 */
	private void validarDuplicidadeCpf(final String cpf) throws RuntimeException {
		if(destinatarioRepository.countByCpf(cpf) > 0) {
			throw new ResponseMessageException(ResponseMessageCode.DUPLICIDADE_CPF);
		}
	}

	/**
	 * Realiza a validação de duplicidade de <b>CPF</b>
	 * 
	 * @param cpfs
	 * @throws RuntimeException
	 */
	private void validarDuplicidadeCpfDestinatario(final List<Destinatario> destinatarios) throws RuntimeException {
		List<String> cpfs = destinatarios.stream().map(Destinatario::getCpf).collect(Collectors.toList());

		if(destinatarioRepository.countByCpfList(cpfs) > 0) {
			throw new ResponseMessageException(ResponseMessageCode.DUPLICIDADE_CPF);
		}
	}

	/**
	 * Realiza a validação de duplicidade de <b>Email</b>
	 * 
	 * @param destinatarios
	 * @throws RuntimeException
	 */
	private void validarDuplicidadeEmailDestinatario(final List<Destinatario> destinatarios) throws RuntimeException {
		List<String> emails = destinatarios.stream().map(Destinatario::getEmail).collect(Collectors.toList());

		if(destinatarioRepository.countByEmailList(emails) > 0) {
			throw new ResponseMessageException(ResponseMessageCode.DUPLICIDADE_CPF);
		}
	}

	/**
	 * Realiza a validação de duplicidade de <b>EMAIL</b>
	 * 
	 * @param cpf
	 */
	private void validarDuplicidadeEmail(final String email) {
		if(destinatarioRepository.countByEmail(email) > 0) {
			throw new ResponseMessageException(ResponseMessageCode.DUPLICIDADE_EMAIL);
		}
	}

	/**
	 * Realiza a validação dos digitos de CPF
	 * 
	 * @param cpf
	 */
	private void validarCpf(final String cpf) {
		if(StringUtils.isEmpty(cpf)) {
			throw new ResponseMessageException(ResponseMessageCode.CAMPOS_OBRIGATORIOS_NAO_INFORMADOS);
		}

		if(!CpfUtil.isValid(cpf)) {
			throw new ResponseMessageException(ResponseMessageCode.CPF_INVALIDO);
		}
	}

	/**
	 * Realiza a formatação padrão do <b>CPF</b> de {@link Destinatario}
	 * <dd><i> Ex: 000.000.000-00</i></dd>
	 * 
	 * @param cpf
	 * @return CPF formatado com a mascara padrão
	 */
	private void formatarCpfDestinatario(final Destinatario destinatario) {
		destinatario.setCpf(CpfUtil.formatarCpf(destinatario.getCpf()));
	}

	/**
	 * Realiza a validação de campos obrigatórios da entidade {@link Destinatario}
	 * 
	 * @param destinatario
	 */
	private void validarCamposObrigatorios(final Destinatario destinatario) throws ResponseMessageException {
		if(Objects.isNull(destinatario)
					|| StringUtils.isEmpty(destinatario.getNome())
					|| StringUtils.isEmpty(destinatario.getCpf())
					|| StringUtils.isEmpty(destinatario.getTelefone())
					|| StringUtils.isEmpty(destinatario.getEndereco())
					|| StringUtils.isEmpty(destinatario.getEmail())) {

			throw new ResponseMessageException(ResponseMessageCode.CAMPOS_OBRIGATORIOS_NAO_INFORMADOS);
		}
	}
}
