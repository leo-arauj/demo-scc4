package br.com.scc4.controller;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.scc4.dto.DestinatarioDTO;
import br.com.scc4.mapper.DestinatarioMapper;
import br.com.scc4.model.Destinatario;
import br.com.scc4.service.DestinatarioService;

/**
 * Classe de controle dos Endpoints de {@link Destinatario}
 * 
 * @author Leonardo Araújo
 */
@RestController
@RequestMapping(value = "/destinatarios")
@CrossOrigin(origins = "*")
public class DestinatarioController {

	@Autowired
	private DestinatarioService destinatarioService;
	
	@Autowired
	private DestinatarioMapper destinatarioMapper;


	/**
	 * Realiza a conversão de DTO para Entity e executa o serviço para persistir um
	 * novo {@link Destinatario}
	 * 
	 * @param destinatarioDTO
	 * @return
	 */
	@RequestMapping(produces = "application/json", method = RequestMethod.POST)
	public ResponseEntity<?> salvar(@RequestBody final DestinatarioDTO destinatarioDTO) {
		destinatarioDTO.setId(null);

		Destinatario destinatario = destinatarioMapper.toEntity(destinatarioDTO);
		destinatarioService.salvar(destinatario);

		URI location = getURI(destinatario);

		return ResponseEntity.created(location).build();
	}

	/**
	 * Retorna a {@link DestinatarioDTO} segundo o <b>Email</b> informado
	 * 
	 * @param email
	 * @return
	 */
	@RequestMapping(value = "/email/{email}", method = RequestMethod.GET)
	public ResponseEntity<?> getRemententeByEmail(@PathVariable("email") final String email) {
		Destinatario destinatario = destinatarioService.getDestinatarioByEmail(email);
		DestinatarioDTO destinatarioDTO = destinatarioMapper.toDTO(destinatario);

		return ResponseEntity.ok(destinatarioDTO);
	}

	/**
	 * Endpoint que retorna uma lista com todos os {@link Destinatario}
	 * @return
	 */
	@RequestMapping(produces = "application/json", method = RequestMethod.GET)
	public ResponseEntity<?> getTodosDestinatarios() {
		List<Destinatario> destinatarios = destinatarioService.getTodos();
		List<DestinatarioDTO> destinatariosDTO = destinatarios.stream().map(o -> destinatarioMapper.toDTO(o)).collect(Collectors.toList());

		return ResponseEntity.ok(destinatariosDTO);
	}

	/**
	 * Salva os destinatários contidos no arquivo CSV
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/upload/file", method=RequestMethod.POST)
	public ResponseEntity<?> upload(final MultipartFile file) {
		destinatarioService.salvarDestinatariosOfFileCSV(file);

		return ResponseEntity.ok().build();
	}

	/**
	 * Gera a URI de retorno
	 * 
	 * @param clienteDTO
	 * @return
	 */
	private URI getURI(final Destinatario destinatario) {
		return ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(destinatario.getId()).toUri();
	}
}
