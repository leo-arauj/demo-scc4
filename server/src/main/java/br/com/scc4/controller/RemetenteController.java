package br.com.scc4.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.scc4.dto.RemetenteDTO;
import br.com.scc4.mapper.RemetenteMapper;
import br.com.scc4.model.Remetente;
import br.com.scc4.service.RemetenteService;

/**
 * Classe de controle dos Endpoints de {@link Remetente}
 * 
 * @author Leonardo Araújo
 */
@RestController
@RequestMapping(value = "/remetentes")
@CrossOrigin(origins = "*")
public class RemetenteController {

	@Autowired
	private RemetenteService remetenteService;
	
	@Autowired
	private RemetenteMapper remetenteMapper;


	/**
	 * Realiza a conversão de DTO para Entity e executa o serviço para persistir um
	 * novo {@link Remetente}
	 * 
	 * @param remetenteDTO
	 * @return
	 */
	@RequestMapping(produces = "application/json", method = RequestMethod.POST)
	public ResponseEntity<?> salvar(@RequestBody final RemetenteDTO remetenteDTO) {
		remetenteDTO.setId(null);

		Remetente remetente = remetenteMapper.toEntity(remetenteDTO);
		remetenteService.salvar(remetente);

		URI location = getURI(remetente);

		return ResponseEntity.created(location).build();
	}

	/**
	 * Endpoint que retorna uma lista com todos os {@link Remetente}
	 * 
	 * @return
	 */
	@RequestMapping(produces = "application/json", method = RequestMethod.GET)
	public ResponseEntity<?> getTodosRemetentes() {
		List<Remetente> remetentes = remetenteService.getTodos();
		List<RemetenteDTO> remetentesDTO = remetentes.stream().map(o -> remetenteMapper.toDTO(o)).collect(Collectors.toList());

		return ResponseEntity.ok(remetentesDTO);
	}

	/**
	 * Retorna o {@link RemetenteDTO} segundo ID
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(produces = "application/json", value = "/{id:[\\d]+}", method = RequestMethod.GET)
	public ResponseEntity<?> getRemetenteById(@PathVariable("id") final Long id) {
		Remetente rementente = remetenteService.getRemetenteById(id);
		RemetenteDTO remetenteDTO = remetenteMapper.toDTO(rementente);

		return ResponseEntity.ok(remetenteDTO);
	}

	/**
	 * Endpoint que exclui um {@link Remetente} da base de dados
	 * @param id
	 * @return
	 */
	@RequestMapping(produces = "application/json", value = "/{id:[\\d]+}", method = RequestMethod.DELETE)
	public ResponseEntity<?> excluir(@PathVariable("id") final Long id) {
		remetenteService.excluirRemetente(id);

		return ResponseEntity.noContent().build();
	}

	/**
	 * Realiza a conversão de DTO para Entity e executa o serviço para alterar {@link Remetente}
	 * 
	 * @param remetenteDTO
	 * @return
	 */
	@RequestMapping(produces = "application/json", method = RequestMethod.PUT)
	public ResponseEntity<?> alterar(@RequestBody final RemetenteDTO remetenteDTO) {
		Remetente remetente = remetenteMapper.toEntity(remetenteDTO);
		remetenteService.salvar(remetente);

		return ResponseEntity.accepted().build();
	}

	/**
	 * Retorna a {@link RemetenteDTO} segundo o <b>Email</b> informado
	 * 
	 * @param email
	 * @return
	 */
	@RequestMapping(produces = "application/json", value = "/email/{email}", method = RequestMethod.GET)
	public ResponseEntity<?> getRemententeByEmail(@PathVariable("email") final String email) {
		Remetente rementente = remetenteService.getRemetenteByEmail(email);
		RemetenteDTO remetenteDTO = remetenteMapper.toDTO(rementente);

		return ResponseEntity.ok(remetenteDTO);
	}

	/**
	 * Gera a URI de retorno
	 * 
	 * @param clienteDTO
	 * @return
	 */
	private URI getURI(final Remetente remetente) {
		return ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(remetente.getId()).toUri();
	}
}
