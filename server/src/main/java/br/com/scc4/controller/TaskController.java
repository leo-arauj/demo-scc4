package br.com.scc4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.scc4.service.TaskService;

@RestController
@CrossOrigin(origins = "*")
public class TaskController {

	@Autowired
	private TaskService taskService;

	/**
	 * Recebe uma lista de inteiros e inverte a ordem dos elementos
	 * 
	 * @param lista
	 * @return
	 */
	@RequestMapping(value = "/listaReversa", method = RequestMethod.GET)
	public ResponseEntity<?> getListaReversa(@RequestParam("lista") final List<Integer> lista) {
		taskService.getListaReversa(lista);

		return ResponseEntity.ok(lista);
	}

	/**
	 * Recebe uma lista de inteiros e retorna somente os números impares
	 * 
	 * @param lista
	 * @return
	 */
	@RequestMapping(value = "/imprimirImpares", method = RequestMethod.GET)
	public ResponseEntity<?> getNumerosImpares(@RequestParam("lista") final List<Integer> lista) {
		List<Integer> numerosImpares = taskService.getNumerosImpares(lista);

		return ResponseEntity.ok(numerosImpares);
	}

	/**
	 * Recebe uma lista de inteiros e retorna somente os números pares
	 * 
	 * @param lista
	 * @return
	 */
	@RequestMapping(value = "/imprimirPares", method = RequestMethod.GET)
	public ResponseEntity<?> getNumerosPares(@RequestParam("lista") final List<Integer> lista) {
		List<Integer> numerosPares = taskService.getNumerosPares(lista);

		return ResponseEntity.ok(numerosPares);
	}
	
	/**
	 * Endpoint que recebe uma String como parâmetro e retorna o tamanho dela
	 * 
	 * @param lista
	 * @return
	 */
	@RequestMapping(value = "/tamanho", method = RequestMethod.GET)
	public ResponseEntity<?> getTamanhoPalavra(@RequestParam("palavra") final String palavra) {
		String palavraFormatada = taskService.getTamanhoPalavra(palavra);

		return ResponseEntity.ok(palavraFormatada);
	}

	/**
	 * Endpoint que recebe uma String como parâmetro e retorna uma String com todas as letras maiúsculas
	 * 
	 * @param palavra
	 * @return
	 */
	@RequestMapping(value = "/maisculas", method = RequestMethod.GET)
	public ResponseEntity<?> getPalavraToUpperCase(@RequestParam("palavra") final String palavra) {
		String palavraFormatada = taskService.getPalavraToUpperCase(palavra);

		return ResponseEntity.ok(palavraFormatada);
	}

	/**
	 * Endpoint que recebe uma String como parâmentro e somente as vogais contidas na String
	 * 
	 * @param palavra
	 * @return
	 */
	@RequestMapping(value = "/vogais", method = RequestMethod.GET)
	public ResponseEntity<?> getVogais(@RequestParam("palavra") final String palavra) {
		String palavraFormatada = taskService.getVogais(palavra);

		return ResponseEntity.ok(palavraFormatada);
	}

	/**
	 * Endpoint que recebe uma String como parâmentro e somente as consoantes contidas na String
	 * 
	 * @param palavra
	 * @return
	 */
	@RequestMapping(value = "/consoantes", method = RequestMethod.GET)
	public ResponseEntity<?> getConsoantes(@RequestParam("palavra") final String palavra) {
		String palavraFormatada = taskService.getConsoantes(palavra);

		return ResponseEntity.ok(palavraFormatada);
	}

	/**
	 * Endpoint que recebe um nome e o transforma em nome bibliográfico
	 * 
	 * @param palavra
	 * @return
	 */
	@RequestMapping(value = "/nomeBibliografico", method = RequestMethod.GET)
	public ResponseEntity<?> getNomeBibliografico(@RequestParam("nome") final String nome) {
		String nomeFormatado = taskService.getNomeBibliografico(nome);

		return ResponseEntity.ok(nomeFormatado);
	}
	
	@RequestMapping(value = "/saque", method = RequestMethod.GET)
	public ResponseEntity<?> getNomeBibliografico(@RequestParam("valor") final Integer valor) {
		String nomeFormatado = taskService.getSaque(valor);

		return ResponseEntity.ok(nomeFormatado);
	}


}
