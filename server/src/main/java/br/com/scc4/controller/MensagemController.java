package br.com.scc4.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.scc4.dto.MensagemDTO;
import br.com.scc4.mapper.MensagemMapper;
import br.com.scc4.model.Mensagem;
import br.com.scc4.service.MensagemService;

/**
 * Classe de controle dos Endpoints de {@link Mensagem}
 * 
 * @author Leonardo Araújo
 */
@RestController
@RequestMapping(value = "/mensagens")
@CrossOrigin(origins = "*")
public class MensagemController {

	@Autowired
	private MensagemService mensagemService;
	
	@Autowired
	private MensagemMapper mensagemMapper;


	/**
	 * Realiza a conversão de DTO para Entity e executa o serviço para persistir um
	 * novo {@link Mensagem}
	 * 
	 * @param mensagemDTO
	 * @return
	 */
	@RequestMapping(produces = "application/json", method = RequestMethod.POST)
	public ResponseEntity<?> salvar(@RequestBody final MensagemDTO mensagemDTO) {
		mensagemDTO.setId(null);

		Mensagem mensagem = mensagemMapper.toEntity(mensagemDTO);
		mensagemService.salvar(mensagem);

		URI location = getURI(mensagem);

		return ResponseEntity.created(location).build();
	}

	/**
	 * Gera a URI de retorno
	 * 
	 * @param clienteDTO
	 * @return
	 */
	private URI getURI(final Mensagem mensagem) {
		return ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(mensagem.getId()).toUri();
	}
}
