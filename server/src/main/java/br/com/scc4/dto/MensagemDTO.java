package br.com.scc4.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import br.com.scc4.model.Mensagem;

/**
 * Classe de representação(DTO) da entidade {@link Mensagem}
 * @author Leonardo Araújo
 */
@JsonInclude(content = Include.NON_NULL)
public class MensagemDTO implements Serializable {
	private static final long serialVersionUID = 2950269568797094161L;

	private Long id;
	private RemetenteDTO remetente;
	private List<DestinatarioDTO> destinatarios;
	private String mensagem;


	/**
	 * Construtor da classe
	 */
	public MensagemDTO() {
		
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the remetente
	 */
	public RemetenteDTO getRemetente() {
		return remetente;
	}

	/**
	 * @return the destinatarios
	 */
	public List<DestinatarioDTO> getDestinatarios() {
		return destinatarios;
	}

	/**
	 * @return the mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param remetente the remetente to set
	 */
	public void setRemetente(RemetenteDTO remetente) {
		this.remetente = remetente;
	}

	/**
	 * @param destinatarios the destinatarios to set
	 */
	public void setDestinatarios(List<DestinatarioDTO> destinatarios) {
		this.destinatarios = destinatarios;
	}

	/**
	 * @param mensagem the mensagem to set
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
