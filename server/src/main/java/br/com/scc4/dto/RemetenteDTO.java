package br.com.scc4.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import br.com.scc4.model.Remetente;

/**
 * Classe de representação(DTO) da entidade {@link Remetente}
 * @author Leonardo Araújo
 */
@JsonInclude(content = Include.NON_NULL)
public class RemetenteDTO implements Serializable {
	private static final long serialVersionUID = -4351093004993628406L;

	private Long id;
	private String nome;
	private String cpf;
	private String telefone;
	private String endereco;
	private String email;
	private List<MensagemDTO> mensagens;


	/**
	 * Construtor da classe
	 */
	public RemetenteDTO() {
		
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @return the cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * @return the endereco
	 */
	public String getEndereco() {
		return endereco;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @param cpf the cpf to set
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * @param endereco the endereco to set
	 */
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the mensagens
	 */
	public List<MensagemDTO> getMensagens() {
		return mensagens;
	}

	/**
	 * @param mensagens the mensagens to set
	 */
	public void setMensagens(List<MensagemDTO> mensagens) {
		this.mensagens = mensagens;
	}

	/**
	 * @return the telefone
	 */
	public String getTelefone() {
		return telefone;
	}

	/**
	 * @param telefone the telefone to set
	 */
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

}
