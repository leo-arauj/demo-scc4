package br.com.scc4;

public class Fracao {
	private final long numerador;
	private final long denominador;

	/**
	 * Contrutor da classe
	 * 
	 * @param numerador
	 * @param denominador
	 */
	public Fracao(final long numerador, final long denominador) {
		if (denominador == 0L) {
			throw new IllegalArgumentException("Denominador não pode ser igual a zero.");
		}

		this.numerador = numerador;
		this.denominador = denominador;
	}

	/**
	 * Realiza a soma da fração
	 * 
	 * @param fracao
	 * @return
	 */
	public Fracao somar(final Fracao fracao) {
		long novoDenominador = denominador * fracao.denominador;
		long novoNumerador = acharNovoNumerador(novoDenominador) + fracao.acharNovoNumerador(novoDenominador);

		return new Fracao(novoNumerador, novoDenominador);
	}

	/**
	 * Realiza a subtração da fração
	 * 
	 * @param fracao
	 * @return
	 */
	public Fracao subtrair(final Fracao fracao) {
		long novoDenominador = denominador * fracao.denominador;
		long novoNumerador = acharNovoNumerador(novoDenominador) - fracao.acharNovoNumerador(novoDenominador);

		return new Fracao(novoNumerador, novoDenominador);
	}

	/**
	 * Realiza a multiplicação da fração
	 * 
	 * @param fracao
	 * @return
	 */
	public Fracao mutiplicar(final Fracao fracao) {
		long novoNumerador = numerador * fracao.numerador;
		long novoDenominador = denominador * fracao.denominador;
		
		return new Fracao(novoNumerador, novoDenominador);
	}

	/**
	 * Realiza a divisão da fração
	 * 
	 * @param fracao
	 * @return
	 */
	public Fracao dividir(final Fracao fracao) {
		long novoNumerador = numerador * fracao.denominador;
		long novoDenominador = denominador * fracao.numerador;

		return new Fracao(novoNumerador, novoDenominador);
	}

	/**
	 * Realiza o calculo para encontrar o novo numerador
	 * 
	 * @param novoDenomidador
	 * @return
	 */
	private long acharNovoNumerador(final long novoDenomidador) {
		return novoDenomidador / denominador * numerador;
	}

	/**
	 * Devolve o resultado simplificado da fração
	 * 
	 * @return
	 */
	public Fracao simplificar() {
		long mmc = mmc(numerador, denominador);
		return new Fracao(numerador / mmc, denominador / mmc);
	}

	/**
	 * Retorna o minimo divisor comum entre dois números
	 * 
	 * @param numerador
	 * @param denominador
	 * @return
	 */
	private long mmc(long numerador, long denominador) {
		while(denominador != 0) {
			long temp = denominador;
			denominador = numerador % denominador;
			numerador = temp;
		}

		return numerador;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append("Numerador: ");
		builder.append(numerador);

		builder.append(" / Denominador: ");
		builder.append(denominador);

		return  builder.toString();
	}
}
