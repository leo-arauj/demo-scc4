package br.com.scc4.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.scc4.model.Remetente;

/**
 * Interface de comunicação com o banco referente a entidade {@link Remetente}
 * 
 * @author Leonardo Araújo
 */
@Repository
public interface RemetenteRepository extends JpaRepository<Remetente, Long> {

	/**
	 * Realiza a contagem de CPF's repetidos para validação de duplicidade
	 * 
	 * @param cpf
	 * @return
	 */
	public Long countByCpf(final String cpf);

	/**
	 * Realiza a contagem de Emails repetidos para validação de duplicidade
	 * 
	 * @param email
	 * @return
	 */
	public Long countByEmail(final String email);

	/**
	 * Busca um remetente por Email
	 * 
	 * @param email
	 * @return
	 */
	public Remetente findByEmail(final String email);
}
