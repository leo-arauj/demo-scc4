package br.com.scc4.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.scc4.model.Destinatario;

/**
 * Interface de comunicação com o banco referente a entidade {@link Destinatario}
 * 
 * @author Leonardo Araújo
 */
@Repository
public interface DestinatarioRepository extends JpaRepository<Destinatario, Long> {

	/**
	 * Realiza a contagem de CPF's repetidos
	 * 
	 * @param cpf
	 * @return
	 */
	public Long countByCpf(final String cpf);

	/**
	 * Realiza a contagem de Emails repetidos
	 * 
	 * @param email
	 * @return
	 */
	public Long countByEmail(final String email);

	/**
	 * Realiza a contagem dos CPF's da Lista que estão repetidos
	 * @param cpfs
	 * @return
	 */
	@Query(" SELECT COUNT(destinatario) FROM Destinatario destinatario WHERE destinatario.cpf IN (:cpfs)")
	public Long countByCpfList(@Param("cpfs") final List<String> cpfs);

	/**
	 * Realiza a contagem dos Email's da Lista que estão repetidos
	 * @param emails
	 * @return
	 */
	@Query(" SELECT COUNT(destinatario) FROM Destinatario destinatario WHERE destinatario.email IN (:emails)")
	public Long countByEmailList(@Param("emails") final List<String> emails);

	/**
	 * Busca destinatário por Email
	 * 
	 * @param email
	 * @return
	 */
	public Destinatario findByEmail(final String email);
}
