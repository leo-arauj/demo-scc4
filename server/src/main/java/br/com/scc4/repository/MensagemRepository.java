package br.com.scc4.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.scc4.model.Mensagem;

/**
 * Interface de comunicação com o banco referente a entidade {@link Mensagem}
 * @author Leonardo Araújo
 */
@Repository
public interface MensagemRepository extends JpaRepository<Mensagem, Long> {

}
