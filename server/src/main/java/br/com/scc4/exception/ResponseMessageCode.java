package br.com.scc4.exception;

import org.springframework.http.HttpStatus;

/**
 * Enum com possivéis respostas do servidor e seus respectivos {@link HttpStatus}
 * 
 * @author Leonardo Araújo
 */
public enum ResponseMessageCode {

	CAMPOS_OBRIGATORIOS_NAO_INFORMADOS(400, "Campos obrigatórios não informados."),
	NENHUM_RESULTADO_ENCONTRADO(404, "Nenhum resultado encontrado"),
	CPF_INVALIDO(400, "CPF inválido"),
	DUPLICIDADE_CPF(409, "CPF já cadastrado"),
	DUPLICIDADE_CPF_CSV(409, "Existem CPF's repetidos no documento de importação CSV"),
	DUPLICIDADE_EMAIL(409, "Email já cadastrado"),
	DUPLICIDADE_EMAIL_CSV(409, "Existem e-mails repetidos no documento de importação CSV"),
	ARQUIVO_NAO_PADRAO_CSV(400, "O arquivo deve estar no formado CSV"),
	ERRO_LEITURA_ARQUIVO_CSV(400, "Arquivo não esta no padrão correto para importação de destinatarios!");

	private int codeStatus;
	private String message;

	/**
	 * Constructor
	 * 
	 * @param status
	 * @param message
	 */
	private ResponseMessageCode(final int status, final String message) {
		this.codeStatus = status;
		this.message = message;
	}
	
	/**
	 * @return the status
	 */
	public int getCodeStatus() {
		return codeStatus;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
}
