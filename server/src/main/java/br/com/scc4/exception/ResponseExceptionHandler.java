package br.com.scc4.exception;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.scc4.response.HttpResponseMessage;

/**
 * EndPoint que manipula as mensagens de resposta em caso de {@link Exception}
 * 
 * @author Leonardo Araújo
 */
@ControllerAdvice
@RestController
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

	@ResponseBody
	@ExceptionHandler(ResponseMessageException.class)
	public final ResponseEntity<HttpResponseMessage> handleAllExceptions(final ResponseMessageException exceptionResponse, final WebRequest request) {

		HttpStatus status = HttpStatus.valueOf(exceptionResponse.getStatus());

		HttpResponseMessage response = new HttpResponseMessage();

		response.setCode(status.value());
		response.setStatus(status);
		response.setMessage(exceptionResponse.getMessage());
		response.setDate(LocalDateTime.now());
		response.setPath(request.getDescription(false));

		return new ResponseEntity<HttpResponseMessage>(response, status);
	}
}
