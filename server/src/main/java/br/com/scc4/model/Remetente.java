package br.com.scc4.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name = "REMETENTE")
@JsonInclude(content = Include.NON_NULL)
public class Remetente implements Serializable {
	private static final long serialVersionUID = 6771455425246618566L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_REMETENTE")
	private Long id;

	@Column(name = "NOME", nullable = false)
	private String nome;

	@Column(name = "CPF", nullable = false, unique = true, length = 14)
	private String cpf;

	@Column(name = "TELEFONE", nullable = false)
	private String telefone;

	@Column(name = "ENDERECO", nullable = false)
	private String endereco;

	@Column(name = "EMAIL", nullable = false, unique = true)
	private String email;

	@OneToMany(mappedBy = "remetente", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	private List<Mensagem> mensagens;

	/**
	 * Construtor da classe
	 */
	public Remetente() {
		
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @return the cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * @return the endereco
	 */
	public String getEndereco() {
		return endereco;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the mensagens
	 */
	public List<Mensagem> getMensagens() {
		return mensagens;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param mensagens the mensagens to set
	 */
	public void setMensagens(List<Mensagem> mensagens) {
		this.mensagens = mensagens;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @param cpf the cpf to set
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * @param endereco the endereco to set
	 */
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the telefone
	 */
	public String getTelefone() {
		return telefone;
	}

	/**
	 * @param telefone the telefone to set
	 */
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

}
