package br.com.scc4.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name = "MENSAGEM")
@JsonInclude(content = Include.NON_NULL)
public class Mensagem implements Serializable {
	private static final long serialVersionUID = -2243078914290562965L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_MENSAGEM")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "FK_REMETENTE")
	private Remetente remetente;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		name = "MENSAGEM_DESTINATARIO",
		uniqueConstraints = {@UniqueConstraint(columnNames = {"FK_MENSAGEM", "FK_DESTINATARIO"})},
		joinColumns = @JoinColumn(name = "FK_MENSAGEM", referencedColumnName = "PK_MENSAGEM", nullable = false),
		inverseJoinColumns = @JoinColumn(name = "FK_DESTINATARIO", referencedColumnName = "PK_DESTINATARIO", nullable = false)
	)
	private List<Destinatario> destinatarios;

	@Column(name = "MENSAGEM", nullable = false)
	private String mensagem;

	/**
	 * Construtor da classe
	 */
	public Mensagem() {
		
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the remetente
	 */
	public Remetente getRemetente() {
		return remetente;
	}

	/**
	 * @return the destinatarios
	 */
	public List<Destinatario> getDestinatarios() {
		return destinatarios;
	}

	/**
	 * @return the mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param remetente the remetente to set
	 */
	public void setRemetente(Remetente remetente) {
		this.remetente = remetente;
	}

	/**
	 * @param destinatarios the destinatarios to set
	 */
	public void setDestinatarios(List<Destinatario> destinatarios) {
		this.destinatarios = destinatarios;
	}

	/**
	 * @param mensagem the mensagem to set
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
