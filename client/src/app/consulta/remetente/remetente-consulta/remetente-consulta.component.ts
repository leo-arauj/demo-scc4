import { RemetenteService } from './../../../service/remetente/remetente.service';
import { Component, OnInit } from '@angular/core';
import { Remetente } from 'src/app/model/remetente';
import { MatSnackBar, MatDialog, MatDialogConfig } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil, filter, tap } from 'rxjs/operators';
import { RemetenteEditarComponent } from './remetente-editar/remetente-editar.component';

@Component({
  selector: 'app-remetente-consulta',
  templateUrl: './remetente-consulta.component.html',
  styleUrls: ['./remetente-consulta.component.css']
})
export class RemetenteConsultaComponent implements OnInit {

  remetentes: Remetente[] = []

  displayedColumns: string[] = ['nome', 'cpf', 'telefone', 'endereco', 'email', 'acao'];
  private unsubscribe$: Subject<any> = new Subject<any>()

  constructor(
    private remetenteService: RemetenteService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.remetenteService.get()
      .pipe(
        takeUntil(this.unsubscribe$),
        tap(res => this.remetentes = res)
      )
      .subscribe(
        (res) => this.remetentes = res,
        (err) => this.notify(err.error.message)
      )
  }

  openDialog(rementente: Remetente): void {
    const dialogRef = this.dialog.open(RemetenteEditarComponent, {
      width: '600px',
      data: rementente
    });

    dialogRef.afterClosed()
    .subscribe(
      (res) => {
        if(res) {
          let index = this.remetentes.findIndex(r => r.id === res.id)
          this.remetentes[index] = res
          this.remetentes = this.remetentes.filter(r => r.id != null)
        }
      }
    )
  }

  deletar(remetente: Remetente) {
    this.remetenteService.delete(remetente)
      .subscribe(
        () => {
          this.remetentes = this.remetentes.filter(r => r.id != remetente.id)
          this.notify("Remetente excluido com sucesso!")
        },
        (err) => this.notify(err.error.message)
      )
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next()
  }

  notify(msg: string) {
    this.snackBar.open(msg, "OK", {duration: 3000})
  }
}
