import { RemetenteService } from './../../../../service/remetente/remetente.service';
import { Validators, FormGroup, FormBuilder, NgForm } from '@angular/forms';
import { Remetente } from './../../../../model/remetente';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-remetente-editar',
  templateUrl: './remetente-editar.component.html',
  styleUrls: ['./remetente-editar.component.css']
})
export class RemetenteEditarComponent implements OnInit {

  private remetenteEditado: Remetente

  remetenteForm: FormGroup = this.formBuilder.group({
    id: [''],
    nome: ['', [Validators.required, Validators.minLength(3)]],
    cpf: ['', [Validators.required]],
    telefone: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    endereco: ['', [Validators.required]],
    mensagens: [[]]
  })

  @ViewChild('form', {static: false}) form: NgForm

  constructor(
    public dialogRef: MatDialogRef<RemetenteEditarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Remetente,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private remetenteService: RemetenteService
    ) {}

  onNoClick(): void {
    this.dialogRef.close(this.remetenteEditado)
    this.resetForm()
  }

  ngOnInit() {
    console.log(this.data)
    this.remetenteForm.setValue(this.data)
  }

  save(){
    this.remetenteService.update(this.remetenteForm.value)
    .subscribe(() => {
          this.remetenteEditado = this.remetenteForm.value
          this.notify("Remetente atualizado com sucesso!")
          this.onNoClick()
        },
        (err) => this.notify(err.error.message)
      )
  }

  resetForm() {
    this.form.resetForm()
  }

  notify(msg: string) {
    this.snackBar.open(msg, "OK", {duration: 3000})
  }
}
