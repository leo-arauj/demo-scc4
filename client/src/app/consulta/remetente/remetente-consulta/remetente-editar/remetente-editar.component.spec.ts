import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemetenteEditarComponent } from './remetente-editar.component';

describe('RemetenteEditarComponent', () => {
  let component: RemetenteEditarComponent;
  let fixture: ComponentFixture<RemetenteEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemetenteEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemetenteEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
