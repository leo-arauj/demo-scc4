import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemetenteConsultaComponent } from './remetente-consulta.component';

describe('RemetenteConsultaComponent', () => {
  let component: RemetenteConsultaComponent;
  let fixture: ComponentFixture<RemetenteConsultaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemetenteConsultaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemetenteConsultaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
