import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinatarioConsultaComponent } from './destinatario-consulta.component';

describe('DestinatarioConsultaComponent', () => {
  let component: DestinatarioConsultaComponent;
  let fixture: ComponentFixture<DestinatarioConsultaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinatarioConsultaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinatarioConsultaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
