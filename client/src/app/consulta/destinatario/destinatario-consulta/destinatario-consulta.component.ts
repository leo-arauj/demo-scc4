import { DestinatarioService } from './../../../service/destinatario/destinatario.service';
import { Destinatario } from './../../../model/destinatario';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { MatSnackBar, MatDialog } from '@angular/material';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-destinatario-consulta',
  templateUrl: './destinatario-consulta.component.html',
  styleUrls: ['./destinatario-consulta.component.css']
})
export class DestinatarioConsultaComponent implements OnInit {

  destinatarios: Destinatario[] = []

  displayedColumns: string[] = ['nome', 'cpf', 'telefone', 'endereco', 'email'];
  private unsubscribe$: Subject<any> = new Subject<any>()

  constructor(
    private destinatarioService: DestinatarioService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.destinatarioService.get()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (res) => this.destinatarios = res,
        (err) => this.notify(err.error.message)
      )
  }


  ngOnDestroy(): void {
    this.unsubscribe$.next()
  }

  notify(msg: string) {
    this.snackBar.open(msg, "OK", {duration: 3000})
  }
}
