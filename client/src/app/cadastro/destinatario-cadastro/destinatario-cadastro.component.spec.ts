import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinatarioCadastroComponent } from './destinatario-cadastro.component';

describe('DestinatarioCadastroComponent', () => {
  let component: DestinatarioCadastroComponent;
  let fixture: ComponentFixture<DestinatarioCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinatarioCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinatarioCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
