import { DestinatarioService } from './../../service/destinatario/destinatario.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-destinatario-cadastro',
  templateUrl: './destinatario-cadastro.component.html',
  styleUrls: ['./destinatario-cadastro.component.css']
})
export class DestinatarioCadastroComponent implements OnInit {

  @ViewChild('inputFile', {static: false})
  myInputVariable: ElementRef;

  fileToUpload: File = null

  validatedFile: boolean = false

  constructor(
    private destinatarioService: DestinatarioService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
  }

  handleFileInput(file: FileList) {
    if(file.length > 0) {
      let array = file[0].name.split('.')
      let contentType = array[array.length - 1]
  
      // if(event.target.files && event.target.files[0]) {
      if(contentType === 'csv') {
        this.fileToUpload  = file[0]
        this.validatedFile = true;
      } else {
        this.notify("Arquivo não esta no formato CSV", 3000)
      }
    }
  }

  uploadFileToActivity() {
    if(this.validatedFile) {
      const formData = new FormData()
      formData.append('file', this.fileToUpload )
  
      this.destinatarioService.saveDestinatariosByFileCSV(formData)
        .subscribe(
          () => {
            this.notify("Destinatários cadastrados com sucesso!", 3000)
            this.router.navigate(['/consultaDestinatario'], {relativeTo: this.route})
          },
          (err) => {
            this.myInputVariable.nativeElement.value = "";
            this.validatedFile = false;
            this.notify(err.error.message, 5000)
          }
        )
    }
  }

  notify(msg: string, time: number) {
    this.snackBar.open(msg, "OK", {duration: 3000})
  }
}
