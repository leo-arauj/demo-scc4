import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemetenteCadastroComponent } from './remetente-cadastro.component';

describe('RemetenteCadastroComponent', () => {
  let component: RemetenteCadastroComponent;
  let fixture: ComponentFixture<RemetenteCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemetenteCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemetenteCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
