import { RemetenteService } from './../../service/remetente/remetente.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-remetente-cadastro',
  templateUrl: './remetente-cadastro.component.html',
  styleUrls: ['./remetente-cadastro.component.css']
})
export class RemetenteCadastroComponent implements OnInit {

  remetenteForm: FormGroup = this.formBuilder.group({
    id: [null],
    nome: ['', [Validators.required, Validators.minLength(3)]],
    cpf: ['', [Validators.required]],
    telefone: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    endereco: ['', [Validators.required]]
  })

  @ViewChild('form', {static: true}) form: NgForm

  private unsubscribe$: Subject<any> = new Subject<any>()

  constructor(
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private remetenteService: RemetenteService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.unsubscribe$.next()
  }

  save() {
    this.remetenteService.save(this.remetenteForm.value)
      .subscribe(
        () => {
          this.notify("Remetente cadastrado com sucesso!")
          this.resetForm()
        },
        (err) => this.notify(err.error.message)
      )
  }

  cancelar() {
    this.router.navigate(['/consultaRemetente'], {relativeTo: this.route})
  }

  resetForm() {
    this.form.resetForm()
  }

  notify(msg: string) {
    this.snackBar.open(msg, "OK", {duration: 3000})
  }
}
