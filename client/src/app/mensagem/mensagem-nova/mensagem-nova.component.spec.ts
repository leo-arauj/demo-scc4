import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensagemNovaComponent } from './mensagem-nova.component';

describe('MensagemNovaComponent', () => {
  let component: MensagemNovaComponent;
  let fixture: ComponentFixture<MensagemNovaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensagemNovaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensagemNovaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
