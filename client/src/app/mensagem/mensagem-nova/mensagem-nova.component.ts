import { DestinatarioService } from './../../service/destinatario/destinatario.service';
import { RemetenteService } from './../../service/remetente/remetente.service';
import { MensagemService } from './../../service/mensagem/mensagem.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, NgForm, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil, tap } from 'rxjs/operators';
import { Remetente } from 'src/app/model/remetente';
import { Destinatario } from 'src/app/model/destinatario';

@Component({
  selector: 'app-mensagem-nova',
  templateUrl: './mensagem-nova.component.html',
  styleUrls: ['./mensagem-nova.component.css']
})
export class MensagemNovaComponent implements OnInit {

  mensagemForm: FormGroup = this.formBuilder.group({
    id: [null],
    mensagem: ['', [Validators.required, Validators.maxLength(300)]],
    remetente: ['', [Validators.required]],
    destinatarios: ['', [Validators.required]],
  })
  
  @ViewChild('form', {static: true}) form: NgForm
  private unsubscribe$: Subject<any> = new Subject<any>()

  remetentes: Remetente[] = []
  destinatarios: Destinatario[] = []

  constructor(
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private mensagemService: MensagemService,
    private remetenteService: RemetenteService,
    private destinatarioService: DestinatarioService
    ) { }

  ngOnInit() {
    this.remetenteService.get()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => this.remetentes = res)
    
    this.destinatarioService.get()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => this.destinatarios = res)
  }

  ngOnDestroy() {
    this.unsubscribe$.next()
  }

  send() {
    this.mensagemService.send(this.mensagemForm.value)
      .subscribe(
        () => {
          this.notify("Mensagem enviada com sucesso!")
          this.resetForm()
        },
        (err) => this.notify(err.error.message)
      )
  }

  cancelar() {
    this.router.navigate(['/consultaRemetente'], {relativeTo: this.route})
  }

  notify(msg: string) {
    this.snackBar.open(msg, "OK", {duration: 3000})
  }

  resetForm() {
    this.form.resetForm()
  }

}
