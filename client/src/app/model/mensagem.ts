import { Destinatario } from './destinatario';
import { Remetente } from './remetente';

export interface Mensagem {
    id?: number;
    mensagem: string;
    remetente: Remetente;
    destinatarios: Destinatario[];
}