export interface Destinatario {
    id?: number;
    nome: string;
    cpf: string;
    endereco: string;
    email: string;
}