import { Mensagem } from './mensagem';

export interface Remetente {
    id?: number;
    nome: string;
    cpf: string;
    endereco: string;
    email: string;
    telefone: string;
    mensages?: Mensagem[];
}