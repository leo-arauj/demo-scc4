import { MensagemNovaComponent } from './mensagem/mensagem-nova/mensagem-nova.component';
import { DestinatarioConsultaComponent } from './consulta/destinatario/destinatario-consulta/destinatario-consulta.component';
import { RemetenteConsultaComponent } from './consulta/remetente/remetente-consulta/remetente-consulta.component';
import { DestinatarioCadastroComponent } from './cadastro/destinatario-cadastro/destinatario-cadastro.component';
import { RemetenteCadastroComponent } from './cadastro/remetente-cadastro/remetente-cadastro.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path: 'cadastroRemetente', component: RemetenteCadastroComponent},
  {path: 'cadastroDestinatario', component: DestinatarioCadastroComponent},
  {path: 'consultaRemetente', component: RemetenteConsultaComponent},
  {path: 'consultaDestinatario', component: DestinatarioConsultaComponent},
  {path: 'mensagem', component: MensagemNovaComponent},
  {path: '', pathMatch: 'full', redirectTo: 'consultaRemetente'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
