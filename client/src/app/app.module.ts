import { MaterialModule } from './material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxMaskModule } from 'ngx-mask';
import { RemetenteCadastroComponent } from './cadastro/remetente-cadastro/remetente-cadastro.component';
import { DestinatarioCadastroComponent } from './cadastro/destinatario-cadastro/destinatario-cadastro.component';
import { RemetenteConsultaComponent } from './consulta/remetente/remetente-consulta/remetente-consulta.component';
import { MatDialogModule } from '@angular/material';
import { RemetenteEditarComponent } from './consulta/remetente/remetente-consulta/remetente-editar/remetente-editar.component';
import { DestinatarioConsultaComponent } from './consulta/destinatario/destinatario-consulta/destinatario-consulta.component';
import { MensagemNovaComponent } from './mensagem/mensagem-nova/mensagem-nova.component';

@NgModule({
  declarations: [
    AppComponent,
    RemetenteCadastroComponent,
    DestinatarioCadastroComponent,
    RemetenteConsultaComponent,
    RemetenteEditarComponent,
    DestinatarioConsultaComponent,
    MensagemNovaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgxMaskModule.forRoot(),
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [RemetenteEditarComponent]
})
export class AppModule { }
