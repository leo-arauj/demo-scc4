import { Injectable } from '@angular/core';
import { Mensagem } from 'src/app/model/mensagem';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MensagemService {

  readonly url = 'http://localhost:8080/mensagens'
  private mensagensSubjects$: BehaviorSubject<Mensagem[]> = new BehaviorSubject<Mensagem[]>(null)
  public mensagens$ = this.mensagensSubjects$.asObservable()

  constructor(private http: HttpClient) { }

  send(mensagem: Mensagem): Observable<Mensagem> {
    return this.http.post<Mensagem>(this.url, mensagem)
  }
}
