import { Destinatario } from './../../model/destinatario';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DestinatarioService {

  readonly url = 'http://localhost:8080/destinatarios'
  private destinatarioSubjects$: BehaviorSubject<Destinatario[]> = new BehaviorSubject<Destinatario[]>(null)
  public destinatarios$ = this.destinatarioSubjects$.asObservable()

  constructor(private http: HttpClient) { }

  get(): Observable<Destinatario[]> {
    return this.http.get<Destinatario[]>(this.url)
  }

  save(destinatario: Destinatario): Observable<Destinatario> {
    return this.http.post<Destinatario>(this.url, destinatario)
  }

  saveDestinatariosByFileCSV(formData: FormData): Observable<any> {
    return this.http.post(`${this.url}/upload/file`, formData)
  }

  delete(destinatario: Destinatario): Observable<Destinatario> {
    return this.http.delete<Destinatario>(`${this.url}/${destinatario.id}`)
  }

  update(destinatario: Destinatario): Observable<Destinatario> {
    return this.http.put<Destinatario>(this.url, destinatario)
  }
}
