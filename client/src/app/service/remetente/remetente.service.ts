import { Remetente } from 'src/app/model/remetente';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { tap, filter, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RemetenteService {

  readonly url = 'http://localhost:8080/remetentes'
  private remetenteSubjects$: BehaviorSubject<Remetente[]> = new BehaviorSubject<Remetente[]>(null)
  public remetentes$ = this.remetenteSubjects$.asObservable()

  constructor(private http: HttpClient) { }

  get(): Observable<Remetente[]> {
    return this.http.get<Remetente[]>(this.url)
  }

  save(remetente: Remetente): Observable<Remetente> {
    return this.http.post<Remetente>(this.url, remetente)
  }

  delete(remetente: Remetente): Observable<Remetente> {
    return this.http.delete<Remetente>(`${this.url}/${remetente.id}`)
  }

  update(remetente: Remetente): Observable<Remetente> {
    return this.http.put<Remetente>(this.url, remetente)
  }
}
