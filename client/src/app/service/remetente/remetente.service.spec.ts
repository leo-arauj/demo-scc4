import { TestBed } from '@angular/core/testing';

import { RemetenteService } from './remetente.service';

describe('RemetenteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RemetenteService = TestBed.get(RemetenteService);
    expect(service).toBeTruthy();
  });
});
